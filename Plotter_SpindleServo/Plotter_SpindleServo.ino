

/*
  Controlling a servo position using a potentiometer (variable resistor)
 by Michal Rinott <http://people.interaction-ivrea.it/m.rinott>
 
 modified on 8 Nov 2013
 by Scott Fitzgerald
 http://www.arduino.cc/en/Tutorial/Knob
 */

#include <Servo.h>

Servo myservo1;  // create servo object to control a servo

int SpindleDir;

int SpindleDirPin = 6;// D2
int Servo1Val = 0;

int minServo = 80;
int maxServo = 120;

void setup() {
  myservo1.attach(9); // D9
  pinMode( SpindleDirPin, INPUT);
  Serial.begin(115200); // Debug
}

void loop() {
  SpindleDir = digitalRead(SpindleDirPin);

  if (HIGH == SpindleDir) {
    myservo1.write( minServo);                  // sets the servo position according to the scaled value
    Serial.print( " S1:");
    Serial.print( minServo);
  } else if {
    myservo1.write( maxServo);                  // sets the servo position according to the scaled value
    Serial.print( " S1:");
    Serial.print( maxServo);
  }


  delay(15);                           // waits for the servo to get there

}

