class GCodeArc {

  PVector point1;
  PVector point2;
  PVector side1;
  PVector side2;
  PVector circleCenter;
  float a;
  
  float diam = 2000;
  int modulation;

  boolean upsidedown;

  GCodeArc(PVector _point1, PVector _point2, boolean _upsidedown) {

    point1 = _point1;
    point2 = _point2;
    upsidedown = _upsidedown;
    modulation = int(random(200));
    
    // Wheris the center of segment
    float centerX = lerp(point1.x, point2.x, 0.5); 
    float centerY = lerp(point1.y, point2.y, 0.5);
    PVector center = new PVector (centerX, centerY);

    PVector n = new PVector();
    float v1;
    float v2;

    // Perpendicular vector
    v1 = point2.x-point1.x;
    v2 = point2.y-point1.y;
    n = new PVector (-v2, v1);
    n.normalize();
    if (upsidedown)
    {
      n.mult(-1);
    }
    n = n.mult(diam+modulation);

    PVector s = new PVector (v1, v2);

    // Get center of circle
    circleCenter = new PVector(center.x+n.x, center.y+n.y);

    // Sides of the pie
    // Side 1
    float side1x = point1.x - circleCenter.x;
    float side1y = point1.y - circleCenter.y;
    side1 = new PVector (side1x, side1y);

    // Side 2
    float side2x = point2.x - circleCenter.x;
    float side2y = point2.y - circleCenter.y;
    side2 = new PVector (side2x, side2y);


    // Get angle
    a = PVector.angleBetween(side1, side2);


    // Get new Diam
    diam = mag(side1.x, side1.y);

    ///////////////////////////////////////////////////////
    //Debug
    /*
    stroke (0);
     line(point1.x, point1.y, point2.x, point2.y);
     
     stroke (255, 0, 0);
     line(center.x, center.y, center.x+n.x, center.y+n.y);
     
     stroke (0, 255, 0);
     line(point1.x, point1.y, center.x+n.x, center.y+n.y);
     line(point2.x, point2.y, center.x+n.x, center.y+n.y);
     
     ellipseMode(CENTER);
     noFill();
     strokeWeight(1);
     stroke (255, 0, 0);
     ellipse (circleCenter.x, circleCenter.y, diam*2, diam*2);
     stroke (0, 0, 255);
     line (circleCenter.x, circleCenter.y, circleCenter.x+diam, circleCenter.y);
     
     fill(255,0,0);
     ellipse(point1.x, point1.y, 5,5);
     noFill(); 
     
     */
    
  }
  
  public void drawArc()
  {
    float b;
    stroke (255, 0, 0);
    strokeWeight(2);
    noFill();
    
    if (upsidedown == false)
    {
      // Get offset Angle
      b = PVector.angleBetween(new PVector(1, 0), side1 );
      if (point1.y<circleCenter.y)b=TWO_PI-b;
      arc(circleCenter.x, circleCenter.y, diam*2, diam*2, b, b+a);
    } else {
      b = PVector.angleBetween(new PVector(1, 0), side2 );
      if (point2.y>circleCenter.y)
      {
        arc(circleCenter.x, circleCenter.y, diam*2, diam*2, b, b+a);
      } else {
        arc(circleCenter.x, circleCenter.y, diam*2, diam*2, TWO_PI-b, (TWO_PI-b)+a);
      }
    }
  }
  
  public String getGcode(float ratio)
  {
    
    
    String newLine = System.getProperty("line.separator");
    String theGcode = "G0 X"+(point1.x*ratio)+" Y"+(point1.y*ratio) + newLine +penDown+ newLine + "G2 X"+(point2.x*ratio)+" Y"+(point2.y*ratio)+ " I"+ ((circleCenter.x-point1.x)*ratio) + " J"+ ((circleCenter.y-point1.y)*ratio) +" F2500";
    return theGcode;
  }
  
  
  
}