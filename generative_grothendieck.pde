import gab.opencv.*;
import org.opencv.imgproc.Imgproc;
import java.util.Collections;
import java.util.Calendar;
import toxi.geom.*;
import toxi.processing.*;


// IMPORT
String path = "/Volumes/Stockage/Seafile/GROTHENDIECK/JPG_RAW";
ArrayList<File> allFiles;
boolean testSize = false;
boolean debug = true;
int maxWidth = 3505;
int maxHeight = 4513;
String biggestPicW ="";
String biggestPicH ="";

/*
maxWidth = 3505 - maxHeight = 4513
 biggestPicW = /Volumes/Stockage/Seafile/GROTHENDIECK/JPG_RAW/118/042.jpg - biggestPicH = /Volumes/Stockage/Seafile/GROTHENDIECK/JPG_RAW/84/004.jpg
 */

//OPENCV
PImage src, dst;
OpenCV opencv;
float minArea = 25.0;
ArrayList<Contour> contours;
ArrayList<Contour> polygons;


//GRID
int numberOfRows;
int numberOfCol;
int minnumberOfRows = 15;
int minnumberOfCol = 20;
int maxnumberOfRows = 50;
int maxnumberOfCol = 30;
int minChosenPoints = 2 ;
int maxChosenPoints = 5;
int margin = 30;
int marginbetweenCells = 0;
//Used for selection inside cell
int divider = 70;

int cellWidth;
int cellHeight;
ArrayList <PVector> cellCoordinates = new ArrayList<PVector>();
ArrayList <PVector> thePoints  = new ArrayList<PVector>();
ArrayList<ArrayList<PVector>> chosenPoints = new ArrayList<ArrayList<PVector>>();

//ARCS
boolean arcs = false;
int nberOfGroupArcs = 6;
int maxArcs = 100;
int minArcs = 30;
ArrayList <PVector[]> pointsArcs= new ArrayList <PVector[]>();
ArrayList<GCodeArc> theGcodeArcs = new ArrayList<GCodeArc>();

//Dots
boolean dots = true;
ArrayList <PVector> dotsList= new ArrayList <PVector>();

//Lines
boolean lines = false;
ArrayList <PVector[]> LineList= new ArrayList <PVector[]>();

//GCODE
/////////////////////////////////////////////
String outputFolder = "./data/";
String outputFile = "dots";
int countGcode = 1;

int paperWidth = 370 ;
int paperHeight = 530;

// Paper sheet dimensions (in mm in real life)
int pW = paperWidth;
int pH = paperHeight;

// let's increase them for a bigger preview in Processing
int scaleRatio = 1;
float sr = scaleRatio;
int ppW = pW*scaleRatio;
int ppH = pH*scaleRatio;

// Defines an output file
PrintWriter output;
boolean GCodeExported = false;

// Define Feed rate (stepper motors speed)
float motorFeedSlow = 3250.0f;
float motorFeedFast = 3500.0f;

// arraylist to save lines coordinates
ArrayList<Line2D> pixelLines = new ArrayList<Line2D>();
ArrayList<Line2D> paperLines = new ArrayList<Line2D>();

String penUp = "M3 S1000";
String penDown = "M4 S1000";
boolean isPenDown = false; // used in order not to repeat unnecessarily the penDown command

float GCodeRatio;

/////////////////////////////////////////////

void setup() {
  size(1000, 1000);
  listPics();
  if (debug)println("Nbre of pics :"+allFiles.size());
  if (testSize)
  {
    println ("maxWidth = "+maxWidth+ " - maxHeight = " + maxHeight); 
    println ("biggestPicW = "+biggestPicW+ " - biggestPicH = " + biggestPicH);
  }
  output = createWriter(outputFolder + outputFile+countGcode+".ngc");// for testing purpose
  GCodeInit();
  
  noLoop();
}

void draw() {
  background(255);
  noLoop();
}

void processOnePic()
{
  loop();
  background(0);
  int indexPic = (int)random(allFiles.size());
  File f = allFiles.get(indexPic);
  String stringPic = f.getAbsolutePath();
  PImage src = loadImage(stringPic);
  println("Pic: "+stringPic+" Width: "+ src.width +" Height: "+src.height);

  //OpenCV stuff
  opencv = new OpenCV(this, src);
  /*
  opencv.gray();
   opencv.threshold(70);
   dst = opencv.getOutput();
   */
  Imgproc.cvtColor(opencv.getColor(), opencv.getColor(), Imgproc.COLOR_BGR2Lab);
  opencv.setGray(opencv.getB());
  opencv.adaptiveThreshold(347, 1);
  opencv.dilate();
  opencv.erode();
  opencv.findCannyEdges(25, 75);
  dst = opencv.getOutput();
  contours = opencv.findContours();
  println("found " + contours.size() + " contours");

  /////////////////////////////////////////////////////
  //GRID
  createGrid(src.width, src.height);
  pickPoints(contours);
  /////////////////////////////////////////////////////
  //ARCS
  if (arcs && contours.size()>300 )
  {
    makeArcs(nberOfGroupArcs);
  }
    /////////////////////////////////////////////////////
  //DOTS
  if (dots )
  {
    makeDots();
  }
  /////////////////////////////////////////////////////
  //LINES
  if (lines )
  {
    makeLines();
  }
  /////////////////////////////////////////////////////
  

  // Show pic
  float theRatio = getRatio( src.width, src.height);
  GCodeRatio = getRatioGCode(src.width, src.height);
  scale(theRatio);
  println(theRatio);
  //image(dst, 0, 0);
  // Draw helpers
  drawGrid();
  //drawPoints();
  if (arcs )drawArcs();
  if (dots)drawDots();
  if(lines)drawLines();
  noFill();
  strokeWeight(1);
  //All the points
  for (Contour contour : contours) {
    stroke(0, 255, 0);
    if (contour.area()>minArea)
    {
      //contour.draw();
      for (PVector point : contour.getPoints()) {
        //point(point.x, point.y);
      }
    }
  }
  scale(0.3);
  //Original image
  //image(src, 0, 0);
  
  noLoop();
}

void createGrid(int srcWidth, int srcHeight)
{
  //RAZ
  cellCoordinates.clear();
  chosenPoints.clear();
  thePoints.clear();
  pointsArcs.clear();
  theGcodeArcs.clear();
  dotsList.clear();
  LineList.clear();
  
  numberOfCol = int(random(minnumberOfCol, maxnumberOfCol));
  numberOfRows = int(random(minnumberOfRows, maxnumberOfRows));

  cellWidth = (srcWidth-(margin*2))/numberOfCol;
  cellHeight = (srcHeight-(margin*2))/numberOfRows;

  // Define coordinates of each cell
  for (int i = 0; i<numberOfCol; i++)
  {
    for (int j = 0; j<numberOfRows; j++)
    {
      cellCoordinates.add( new PVector(i*cellWidth+margin+marginbetweenCells, j*cellHeight+margin+marginbetweenCells));
    }
  }
}

void pickPoints(ArrayList<Contour> contours)
{
  // All the points
  for (Contour contour : contours) {
    if (contour.area()>minArea)
    {
      for (PVector point : contour.getPoints()) {
        thePoints.add(point);
      }
    }
  }

  // Then one arraylist by cell, with the points inside
  if (numberOfCol>0 && numberOfRows>0)
  {
    for (int i = 0; i<numberOfCol; i++)
    {
      for (int j = 0; j<numberOfRows; j++)
      {
        PVector myPoint = cellCoordinates.get(i+(j*numberOfCol));
        ArrayList<PVector> mySelection = new ArrayList<PVector>();
        chosenPoints.add(mySelection);
        //Draw thePoints available in the area  
        for (int k=0; k<thePoints.size(); k++) {
          PVector thePoint = thePoints.get(k);
          if (thePoint.x>(int)myPoint.x && thePoint.x<(int)myPoint.x+cellWidth-marginbetweenCells*2 && thePoint.y>(int)myPoint.y && thePoint.y<(int)myPoint.y+cellHeight-marginbetweenCells*2)
          {
            mySelection.add(thePoint);
          }
        }
      }
    }
  }
}


void drawGrid()
{
  for (int i=0; i<cellCoordinates.size(); i++)
  {
    PVector myOrigin = cellCoordinates.get(i);
    noFill();
    strokeWeight(1);
    stroke (200);
    rect(myOrigin.x, myOrigin.y, cellWidth-marginbetweenCells*2, cellHeight-marginbetweenCells*2);
  }
}

void drawPoints()
{
  //println("chosenPoints: "+chosenPoints.size());
  for (int i=0; i<chosenPoints.size(); i++)
  {
    ArrayList<PVector> mySelection = chosenPoints.get(i);
    Collections.shuffle(mySelection);
    //println("chosenPoints inner: "+i+" "+mySelection.size());
    int maxPoints = int(mySelection.size()/divider);
    for (int j=0; j<maxPoints; j++)
    {
      PVector myPoint = mySelection.get(j);
      noStroke();
      fill (0, 0, 0);
      ellipse(myPoint.x, myPoint.y, 3, 3);
    }
  }
}

////////////////////////////////////////////////////////////////////
//ARCS
void makeArcs(int _nberOfGroupArcs)
{
  for (int i= 0; i<_nberOfGroupArcs; i++)
  {
    // Pick 2 cells
    // The two cells have to be different and contain something
    int cell1 = int(random(chosenPoints.size()));
    ArrayList<PVector> pointsCell1 = chosenPoints.get(cell1);
    while (pointsCell1.size() < maxArcs)
    {
      cell1 = int(random(chosenPoints.size()));
      pointsCell1 = chosenPoints.get(cell1);
    }

    int cell2 = int(random(chosenPoints.size()));
    ArrayList<PVector> pointsCell2 = chosenPoints.get(cell2);
    while (cell2 == cell1 || pointsCell2.size() < maxArcs )
    {
      cell2 = int(random(chosenPoints.size()));
      pointsCell2 = chosenPoints.get(cell2);
    }

    println("Cell 1: "+ cell1 +" contains "+ pointsCell1.size());
    println("Cell 2: "+ cell2 +" contains "+ pointsCell2.size());

    Collections.shuffle(pointsCell1);
    Collections.shuffle(pointsCell2);

    int nberOfArcs = int(random(minArcs, maxArcs));
    // Points chosen in cell 1
    for (int j=0; j<nberOfArcs; j++)
    {
      PVector myPoint1 = pointsCell1.get(j);
      PVector myPoint2 = pointsCell2.get(j);
      PVector[] myPointsArc = {myPoint1, myPoint2};
      pointsArcs.add(myPointsArc);
      theGcodeArcs.add(new GCodeArc(myPoint1, myPoint2, true));
    }
  }
}

void drawArcs()
{
  for (int j=0; j<pointsArcs.size(); j++)
  {
    PVector myPoint1 = pointsArcs.get(j)[0];
    PVector myPoint2 = pointsArcs.get(j)[1];
    println("Points of arc "+j + " "+myPoint1.x+ " "+myPoint1.y+ " "+myPoint2.x+ " "+myPoint2.y);
    noStroke();
    fill (255);
    ellipse(myPoint1.x, myPoint1.y, 25, 25);
    fill (0, 0, 255);
    ellipse(myPoint2.x, myPoint2.y, 25, 25);
  }
  for (int k=0; k<theGcodeArcs.size(); k++)
  {
    theGcodeArcs.get(k).drawArc();
  }
}
////////////////////////////////////////////////////////////////////
//DOTS
void makeDots()
{
  for (int i= 0; i<chosenPoints.size(); i++)
  {
    ArrayList<PVector> pointsCell = chosenPoints.get(i);
    Collections.shuffle(pointsCell);
    int nberOfDots = int(pointsCell.size()/divider);
    // Points chosen in cell 
    for (int j=0; j<nberOfDots; j++)
    {
      PVector myPoint = pointsCell.get(j);
      dotsList.add(myPoint);
    }
  }
}

void drawDots()
{
    for (int j=0; j<dotsList.size(); j++)
  {
    fill(0,0,0);
    ellipse(dotsList.get(j).x, dotsList.get(j).y, 2,2);
  }
}
////////////////////////////////////////////////////////////////////
//LINES
void makeLines()
{
  for (int i= 0; i<chosenPoints.size(); i++)
  {
    ArrayList<PVector> pointsCell = chosenPoints.get(i);
    Collections.shuffle(pointsCell);
    int nberOfDots = int(pointsCell.size()/divider);
    // Points chosen in cell 
    for (int j=0; j<nberOfDots; j++)
    {
      PVector myPoint = pointsCell.get(j);
      PVector myPoint2 = new PVector(myPoint.x, myPoint.y+20);
      LineList.add(new PVector[]{myPoint, myPoint2});
    }
  }
}
void drawLines()
{
    for (int j=0; j<LineList.size(); j++)
  {
    stroke(0,0,0);
    line(LineList.get(j)[0].x, LineList.get(j)[0].y, LineList.get(j)[1].x,LineList.get(j)[1].y);
  }
}


float getRatio(int picW, int picH)
{
  float ratio;
  if (picW > picH)
  {
    ratio = float(width)/float(picW);
  } else {
    ratio = float(height)/float(picH);
  }

  return ratio;
}

float getRatioGCode(int picW, int picH)
{
  float ratio;
  if (picW > picH)
  {
    ratio = float(paperWidth)/float(picW);
  } else {
    ratio = float(paperHeight)/float(picH);
  }

  return ratio;
}


/////////////////////////////////////////
//List pics

void listPics()
{
  allFiles = listFilesRecursive(path);
  if (testSize)
  {
    for (File f : allFiles) 
    {
      println (f.getName());
      //Looking for max dimensions
      if (f.isDirectory() == false && f.getName().charAt(0)!= '.')
      {
        String stringPic = f.getAbsolutePath();
        PImage pic = loadImage(stringPic);
        if (pic.width>maxWidth)
        {
          maxWidth = pic.width;
          biggestPicW = f.getAbsolutePath();
        }
        if (pic.height>maxHeight)
        {
          maxHeight = pic.height;
          biggestPicH = f.getAbsolutePath();
        }
      }
    }
  }
}

// Function to get a list of all files in a directory and all subdirectories
ArrayList<File> listFilesRecursive(String dir) {
  ArrayList<File> fileList = new ArrayList<File>(); 
  recurseDir(fileList, dir);
  return fileList;
}

// Recursive function to traverse subdirectories
void recurseDir(ArrayList<File> a, String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    // If you want to include directories in the list
    //a.add(file);  
    File[] subfiles = file.listFiles();
    for (int i = 0; i < subfiles.length; i++) {
      // Call this function on all files in this directory
      recurseDir(a, subfiles[i].getAbsolutePath());
    }
  } else {
    if (file.getName().charAt(0)!='.')a.add(file);
  }
}



/////////////////////////////////////////////////////////////////////////////
//GCODE

void keyPressed()
{
  if (key == ' ')processOnePic();
  //if (key == 'g'||key == 'G')export2Gcode = true;
  if (key == 's' || key == 'S') saveFrame(timestamp()+"_####.png");
  if (key == 'E' || key == 'e') {

    if ( GCodeExported == false) {
      // export GCODE file
      // LINES
      convertPixelLinesToPaperLines();
      for (int i=0; i<paperLines.size (); i++) {
        // iterate through the saved lines coordinates and write them to the GCode file
        Line2D currentLine = paperLines.get(i);
        if (i == 0) {
          // move from the home/origin to the first point with penUP
          output.println(penUp);
          output.println("G0" + " " + "F" + motorFeedFast);
          output.println("G0" + " " + "X" + currentLine.a.x + " " + "Y" + -(currentLine.a.y));          
          // this is the first line that gets drawn
          output.println(penDown);
          output.println("G0" + " " + "F" + motorFeedSlow);
          isPenDown = true;
          output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + -(currentLine.a.y));
          output.println("G1" + " " + "X" + currentLine.b.x + " " + "Y" + -(currentLine.b.y));
          //println(l.a.x + " " +l.a.y);
        } else {
          Line2D previousLine = paperLines.get(i-1);

          //output.println ("--");
          //output.println ("CurrentLine "+ currentLine + " AX " + currentLine.a.x + " AY " + currentLine.a.y );
          //output.println ("PreviousLine "+ previousLine + " BX " + previousLine.b.x + " BY " + previousLine.b.y );
          //output.println ("--");

          if (currentLine.a.x == previousLine.b.x && currentLine.a.y == previousLine.b.y) {
            // the two lines share a vertex, so the pen head is not lifetd
            //output.println("Line " + i + " and line " + (i-1) + " share a vertex");
            if (isPenDown != true) {
              output.println(penDown);
              isPenDown = true;
            }
            output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + -(currentLine.a.y));
            output.println("G1" + " " + "X" + currentLine.b.x + " " + "Y" + -(currentLine.b.y));
          } else {
            // the two lines DO NOT share a vertex, so the pen head IS RAISED
            // the pen head is quickly moved to the next vertex
            output.println(penUp);
            output.println("G0" + " " + "F" + motorFeedFast);
            output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + -(currentLine.a.y));

            // the line is drawn
            output.println(penDown); 
            output.println("G0" + " " + "F" + motorFeedSlow);
            output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + -(currentLine.a.y));
            output.println("G1" + " " + "X" + currentLine.b.x + " " + "Y" + -(currentLine.b.y));
          }
        }
      }
      //
     
      // ARCS
      for (int k=0; k<theGcodeArcs.size(); k++)
      {
        output.println(penUp);
        output.println(theGcodeArcs.get(k).getGcode(GCodeRatio));
        
      } 
      // DOTS
      for (int l=0; l<dotsList.size(); l++)
      {
        output.println(penUp);
        output.println("G1" + " " + "X" + (dotsList.get(l).x*0.2) + " " + "Y" + (dotsList.get(l).y*0.2));
        output.println(penDown);
        output.println("G4" + " " + "P500");
        
      } 
      
      // LINES
      for (int j=0; j<LineList.size(); j++)
      {
        
        output.println(penUp);
        output.println("G0" + " " + "X" + LineList.get(j)[0].x*GCodeRatio + " " + "Y" + LineList.get(j)[0].y*GCodeRatio);
        output.println(penDown);
        output.println("G1" + " " + "X" + LineList.get(j)[1].x*GCodeRatio + " " + "Y" + LineList.get(j)[1].y*GCodeRatio);
        
      }
      
      GCodeEnd();
      GCodeExported = true;
    } else {
      println("ALREADY EXPORTED TO GCODE...");
    }
  }
}

public void GCodeInit() {
  System.out.println("Init");
  output.println("( Made with Processing / Paper size: "  + pW + "x" + pH + "mm )");
  output.println("G21");
  output.println(penUp);
  output.println("G0" + " " + "F" + motorFeedFast + " " + "X0.0" + " " +  "Y0.0"); 
  output.println(" ");
}

public void GCodeEnd() {
  System.out.println("End");
  // writes a footer with the end instructions for the GCode output file
  output.println(" ");
  // G0 Z90.0
  // G0 X0 Y0 => go home
  // M5 => stop spindle
  // M30 => stop execution
  output.println(penUp);
  output.println("G0 X0 Y0");  
  output.println("M5");
  output.println("M30"); 
  // finalize the GCode text file and quits the current Processing Sketch
  output.flush();  // writes the remaining data to the file
  output.close();  // finishes the output file
  println("***************************");
  println("GCODE EXPORTED SUCCESSFULLY");
  println("***************************");
  exit();  // quits the Processing sketch
}

public float fromPixelToPaperValue(float pixelValue, float sr_) {
  // normalize the pixel X,Y coordinates to the real paper sheet dimensions expressed in mm
  float paperValue = (pixelValue/sr_); 
  ;
  return paperValue;
}

public void convertPixelLinesToPaperLines() {

  for (int i=0; i<pixelLines.size (); i++) {
    // converts the coordinates from pixel to paper values, adjusting for a different origin
    Line2D currentLine = pixelLines.get(i);
    /*
      float paperX1 = fromPixelToPaperValue(currentLine.a.x, sr)-pW/sr;
     float paperY1 = fromPixelToPaperValue(currentLine.a.y, sr)-pH/sr;  
     float paperX2  = fromPixelToPaperValue(currentLine.b.x, sr)-pW/sr;
     float paperY2  = fromPixelToPaperValue(currentLine.b.y, sr)-pH/sr;
     */
    float paperX1 = fromPixelToPaperValue(currentLine.a.x, sr);
    float paperY1 = fromPixelToPaperValue(currentLine.a.y, sr);  
    float paperX2  = fromPixelToPaperValue(currentLine.b.x, sr);
    float paperY2  = fromPixelToPaperValue(currentLine.b.y, sr);

    Line2D linePaper = new Line2D(new Vec2D(paperX1, paperY1), new Vec2D(paperX2, paperY2));
    paperLines.add(linePaper);
  }
}


// timestamp
String timestamp() {
  Calendar now = Calendar.getInstance();
  return String.format("%1$ty%1$tm%1$td_%1$tH%1$tM%1$tS", now);
}